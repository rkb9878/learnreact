const redux = require('redux');



// it is reducer
const counterReducer = (state={counter:0}, action)=>{
    console.log(action);
    if (action.type==="increment"){
        return {
            counter :state.counter+1
        };
    } 
    if (action.type==="decrement"){
        return {
            counter :state.counter-1
        };
    }
     return state
}


// redux store
const store = redux.createStore(counterReducer);

// console.log(store.getState())

// subscriber
const counterSubscriber = ()=>{
    const latestState = store.getState();
    console.log(latestState)
}

store.subscribe(counterSubscriber)
store.dispatch({type:'increment'})
store.dispatch({type:'decrement'})